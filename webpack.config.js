// DO NOT convert to ES6
// wranglerjs/mod.rs can't handle it

const path = require('path')
const NodePolyfillPlugin = require("node-polyfill-webpack-plugin")
const webpack = require('webpack')

module.exports = {
  context: path.resolve(__dirname, './'),
  target: 'webworker',
  mode: 'production',
  output: {
    chunkFormat: 'module'
  },
  optimization: {
    usedExports: true,
    minimize: false
  },
  module: {
    rules: [
      {
        include: /node_modules/,
        test: /\.m?js$/,
        type: 'javascript/auto',
      },
    ],
    noParse: /browserfs\.js/
  },
  plugins: [
    new NodePolyfillPlugin(),
    new webpack.ProvidePlugin({ BrowserFS: 'bfsGlobal', process: 'processGlobal', Buffer: 'bufferGlobal' })
  ],
  resolve: {
    alias: {
      util: require.resolve("util"),
      fs: require.resolve('browserfs/dist/shims/fs.js'),
      buffer: require.resolve('browserfs/dist/shims/buffer.js'),
      path: require.resolve( 'browserfs/dist/shims/path.js'),
      processGlobal: require.resolve( 'browserfs/dist/shims/process.js'),
      bufferGlobal: require.resolve('browserfs/dist/shims/bufferGlobal.js'),
      bfsGlobal: require.resolve('browserfs')
    },
  },
}