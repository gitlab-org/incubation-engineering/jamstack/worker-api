import esbuild from 'esbuild'
import { NodeModulesPolyfillPlugin } from '@esbuild-plugins/node-modules-polyfill'

esbuild.build({
  // Necessary for a successful build:
  plugins: [NodeModulesPolyfillPlugin()],
  platform: 'browser',
  conditions: ['node'],

  // Other esbuild options...
})