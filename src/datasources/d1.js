import { SQLDataSource } from "datasource-sql";

export default class D1DataSource extends SQLDataSource {
  async getPokemon(id) {
    return this.knex.select('*').from('pokemon').where({ id })

    // env.DB.get('SELECT * FROM `pokemon` WHERE id=$id',
    // { id })
  }
}
