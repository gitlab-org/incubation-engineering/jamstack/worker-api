import { ApolloServer } from 'apollo-server-cloudflare';
import {
  graphqlCloudflare,
} from 'apollo-server-cloudflare/dist/cloudflareApollo';

import KVCache from '../kv-cache';
import D1 from '../datasources/d1';
import resolvers from '../resolvers';
import typeDefs from '../schema';
import d1Dialect from 'd1-dialect';
import {GraphQLSchema} from "graphql";
import { makeExecutableSchema } from '@graphql-tools/schema';

const knexConfig = {
  client: d1Dialect,
  driverName: 'd1',
  useNullAsDefault: true,
  connection: {}
}

const dataSources = () => ({
  d1: new D1(knexConfig),
})

const kvCache = { cache: new KVCache() }

const createServer = (graphQLOptions) => {
  return  new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    dataSources,
    ...(graphQLOptions.kvCache ? kvCache : {}),
  })
}

export default async (request, graphQLOptions) => {
  const server = createServer(graphQLOptions)
  await server.start()
  return graphqlCloudflare(() => server.createGraphQLServerOptions(request))(
    request,
  )
}

