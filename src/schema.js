import { gql } from 'apollo-server-cloudflare';

export default gql`
  type Pokemon {
    id: ID!
    name: String!
  }

  type Query {
    pokemon(id: ID!): Pokemon
  }
`
