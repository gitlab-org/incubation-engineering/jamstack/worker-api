export default {
  Query: {
    pokemon: async (_source, { id }, { dataSources }) => {
      let res;
      try {
        res = await dataSources.d1.getPokemon(id)
      } catch (e) {
        console.error(e)
      }
      return res
    },
  },
}
